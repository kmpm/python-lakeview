import unittest
from lakeview.lowrance import Reader


class Test_LowranceReader(unittest.TestCase):
    def test_open(self):
        reader = Reader('asdf')
        self.assertEqual('asdf', reader.filepath)


if __name__ == '__main__':
    unittest.main()
